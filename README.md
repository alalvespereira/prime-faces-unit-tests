# Prime Faces Unit Tests

#### Compilar e executar os testes
```
 mvn clean install
```
#### Executar os testes
```
mvn test

Resultado esperado:
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
```
#### Executar o programa:
```
mvn spring-boot:run

Em seguida, acesse
http://localhost:8080/helloworld.xhtml
 ```
 
#### Fonte:
https://codenotfound.com/jsf-primefaces-automated-unit-testing-selenium.html
