package com.alapsystems.primefacesunittests.primefaces;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {

  public String getMessage(String fn, String ln){
    HelloWorld helloWorld = new HelloWorld();
    helloWorld.setFirstName(fn);
    helloWorld.setLastName(ln);
    return helloWorld.showGreeting();
  }

}
