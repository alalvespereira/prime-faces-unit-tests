package com.alapsystems.primefacesunittests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class PrimefacesunittestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimefacesunittestsApplication.class, args);
		log.info("Prime Faces Unit Tests Application started");
	}

}
