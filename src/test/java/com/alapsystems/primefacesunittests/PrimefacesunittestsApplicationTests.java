package com.alapsystems.primefacesunittests;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PrimefacesunittestsApplicationTests extends WebDriverUtil {

	@Test
	void contextLoads() {
		log.info("!.!.! Tests Started !.!.!");
	}

}
