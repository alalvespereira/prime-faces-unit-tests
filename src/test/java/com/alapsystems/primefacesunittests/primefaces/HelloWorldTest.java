package com.alapsystems.primefacesunittests.primefaces;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.alapsystems.primefacesunittests.WebDriverUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class HelloWorldTest extends WebDriverUtil {

  @Test
  public void testSubmit() {
		
    log.info("!.!.! Primefaces Tests Started !.!.!");

    driver.get("http://localhost:8080/helloworld.xhtml");

    HelloWorldPage page = new HelloWorldPage(driver);
    page.submit("Konatus", "Test Unit");

    String expected = "Hello Konatus Test Unit";

    assertThat(page.getGreeting().trim()).isEqualTo(expected);

    log.info("[OK] Test Submit - Expecting: ".concat(expected).concat(" | Received: ").concat(page.getGreeting().trim()));
  }

  @Test
  public void testSubmitService() {
		
    HelloWorldService helloWorldService = new HelloWorldService();

    String expected = "Hello Konatus Test Unit";
    String received = helloWorldService.getMessage("Konatus", "Test Unit").trim();
    assertThat(received).isEqualTo(expected);

    log.info("[OK] Test Submit Service - Expecting: ".concat(expected).concat(" | Received: ").concat(received));
  }

}
